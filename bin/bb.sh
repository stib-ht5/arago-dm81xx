#! /bin/sh
################################################################################
#This script is a wrapper for bitbake command to simplify product build        # 
#procedure                                                                     #
# Author: Andrei Mironenko <amironenko@hanoverdisplays.com>                    #
#                                                                              #
# Copyright(C) 2012 Hanover Displays Ltd.                                      #
# This file is licensed under the terms of the GNU General Public License      #
# version 2. This program  is licensed "as is" without any warranty of any kind#
# whether express or implied.                                                  #
################################################################################

set -eu

################################################################################
#  Global variables                                                            #
################################################################################

# Set OEBASE to where Arago overlay and OpenEmbedded repositories reside
# NOTE: Do NOT place a trailing / on the end of OEBASE.  Doing so will
#       cause the amend class to not find its files.
export OEBASE="$(cd "$(dirname "$0")/../.." && pwd)"

#Product will be build for this machine
MACHINE=

#Product to build
PRODUCT=

#If it's provided then OE builds a specific package
BB_FILE=

#Command to execute
BB_CMD=

#Debug mode. If't set bitbake will produce a lot of log with debug information.
#Use it to pin in build issue with your component
BB_DEBUG=

# force run of specified cmd, regardless of stamp status
BB_FORCE=

# Library build mode
# This flag is used to separate the image and SDK builds. For the SDK builds
# "-dev" postfix must be added to each of the library. DEV type of the library
# package includes all header files for this library. 
LIB_BUILD_MODE=

# Default build environment is development, DEV_FLAG=1
declare DEV_FLAG=1 

#bb.sh possible errors
declare BB_ERR_SWITCH_NO_SUPPORT=-192
declare BB_ERR_EXTRA_ARGUMENT=-193
declare BB_ERR_SANITY_FAILED=-194

#Build mode. If it's set then all built binaries will be copied to release 
#and not user personal folder on amuxi. Only build-manager should use it as 
#it will require super-user access level on amuxi.
BUILD_PURPOSE="$(whoami)"

usage() {
	local SCRIPT="${0##*/}"
	cat <<EOT
This script is a wrapper for bitbake command to simplify
project build procedure in the OE

Usage: $SCRIPT -p <project> -m <machine> [options...]

Available options:
 -b, --bitbake		Build a particular package instead of the whole image
 -c, --command		Run only one bitbake command, e.g. -c clean
 -r, --release		Make a release build
 -f, --freeze		For build manager only. Build a final product release
 -D, --debug		Enable OE extra build information
 -F, --force		Force run of specified cmd, regardless of stamp status.
 -h, --help		This help

Some examples:

To build release version of the project:
  $SCRIPT -p sr1106 -m dm814x-z3 -r"

To build  development version of the product:
  $SCRIPT -p sr1106 -m dm814x-z3

To build one package use -b option:
  $SCRIPT -p sr1106 -m dm814x-z3 -b hanover-apps-dev/recipes/iptft/iptft_git.bb

To clean the same pacakge package:
  $SCRIPT -p sr1106 -m dm814x-z3 -b hanover-apps-dev/recipes/iptft/iptft_git.bb -c clean

EOT
}

while [ $# -gt 0 ]; do
case "$1" in
    --help | -h) 	usage; exit 1; ;;

    --product | -p)	PRODUCT="$2"; shift ;;
    --machine | -m)	MACHINE="$2"; shift ;;

    --bitbake | -b)     BB_FILE="$2"; shift ;;
    --command | -c)     BB_CMD="$2"; shift ;;
    --release | -r)     DEV_FLAG=0 ;;
    --freeze  | -f)     BUILD_PURPOSE="release" DEV_FLAG=0 ;;
    --debug   | -D)     BB_DEBUG=1 ;;
    --force   | -F)     BB_FORCE=1 ;;

    --)			shift; break ;;
    -*)                 echo "$1: option not supported" >&2
	    		usage; exit -1 ;;
    *)                  break ;;
esac
	shift
done

#Sanity check for parameters and build targets
SANITY_CHECK_STATUS=1

#Check mandatory parameters first
if [ -n "$PRODUCT" -a ! -s "$OEBASE/hanover-products/$PRODUCT/conf/layer.conf" ]; then
	echo "$PRODUCT: Invalid PRODUCT layer" >&2
	SANITY_CHECK_STATUS=0
fi

if [ -z "$MACHINE" ] ; then
	echo "Mandatory parameter -m, --machine is missed" >&2
	SANITY_CHECK_STATUS=0
fi

# compose conf/bblayers.conf
#
f="$OEBASE/conf/bblayers.conf"
mkdir -p "$(dirname "$f")"

# automatically promote dev-less workspaces
#
if [ "$DEV_FLAG" = "1" -a \
     ! -e "$OEBASE/hanover-apps-dev/conf/layer.conf" -a \
     ! -e "$OEBASE/hanover-system-dev/conf/layer.conf" ]; then
	DEV_FLAG=0
fi

if [ "$DEV_FLAG" = "1" ] ; then
	RELDIR="dev"
	BBLAYERS="arago-oe-dev arago \
		  hanover-apps hanover-apps-dev \
		  hanover-system hanover-system-dev \
		  "
else
	RELDIR="rel"
	BBLAYERS="arago-oe-dev arago \
		  hanover-apps \
		  hanover-system \
		  "
fi
[ -z "$PRODUCT" ] || BBLAYERS="$BBLAYERS hanover-products/${PRODUCT}"

echo "BBLAYERS = \"\\" > "$f~"
for x in $BBLAYERS; do
	# and verify all directories exist
	if [ ! -d "$OEBASE/$x" ]; then
		echo "$x: layer missing" >&2
		SANITY_CHECK_STATUS=0
	fi

	echo "	\${OEBASE}/$x \\" >> "$f~"
done
echo "\"" >> "$f~"
mv "$f~" "$f"

export HANOVER_SYSTEM_DEV="${OEBASE}/hanover-system-dev"
export HANOVER_SYSTEM="${OEBASE}/hanover-system"
export HANOVER_APPS_DEV="${OEBASE}/hanover-apps-dev"
export HANOVER_APPS="${OEBASE}/hanover-apps"
export ARAGO_TOP="${OEBASE}/arago"
export OE_TOP="${OEBASE}/arago-oe-dev"

if [ "$SANITY_CHECK_STATUS" = "1" ] ; then
	printf "%s\n" "Sanity check for parameters and build targets...Ok"
else
	printf "%s\n" "Sanity check for parameters and build targets...Failed"
	exit -1 
fi

if [ -n "$PRODUCT" ]; then
	HANOVER_PRODUCT="${OEBASE}/hanover-products/${PRODUCT}"
	PRODUCT_RELEASE=`git --git-dir=./hanover-products/${PRODUCT}/.git describe`
else
	HANOVER_PRODUCT=
	PRODUCT_RELEASE=
fi

# find something to call HANOVER_VERSION
# based on the versions of the repositories on the workspace
get_hanover_version() {
	local tag= count= versions=
	local x=

	# git describe returns strings of the form:
	# TAG and TAG-C-gHEAD
	# so first we take the latest TAG
	# and add up all the Cs (changes) to get a grand total
	versions="$(ls -1d .repo/manifests/.git */.git 2> /dev/null |
		sed -e 's|/\.git$||' |
		while read r; do
			git \
			--work-tree "$r" \
			--git-dir "$r/.git" \
			describe
		done | cut -d_ -f2- | sort -rV)"

	tag=$(echo "$versions" | head -n1 | sed -e 's|-[0-9]\+-g[0-9a-f]\+$||')

	count=0
	for x in $(echo "$versions" |
		sed -n -e 's|.*-\([0-9]\+\)-g[0-9a-f]\+$|\1|p'); do
		count=$((count + $x))
	done
	if [ "${count:-0}" != "0" ]; then
		echo "$tag-$count"
	else
		echo "$tag"
	fi
}
HANOVER_VERSION=$(get_hanover_version)

#Check either it is going a complete product or just one component build
if [ -n "$BB_FILE" ]; then
	# TODO: confirm it really exists
	# TODO: make relative
	set -- -b "$BB_FILE" "$@"
elif [ $# -lt 2 ]; then
	case "${1:-}" in
	image|"")
		set --  hanover-${PRODUCT:-base}-image
		;;
	sdk)
		set -- hanover-sdk
		;;
	esac
fi

set -- bitbake -k \
	${BB_FORCE:+ -f} \
	${BB_DEBUG:+ -DDD} \
	${BB_CMD:+ -c "$BB_CMD"} \
	"$@"

BB_ENV_EXTRAWHITE=

cat <<EOT
BITBAKE=$*
EOT

# variables that are passed and shown even if empty
for k in \
	MACHINE PRODUCT \
	HANOVER_VERSION \
	PRODUCT_RELEASE \
	PRODUCT_VERSION \
	RELDIR \
	BUILD_PURPOSE \
	; do

	eval "v=\"\${$k:-}\""
	BB_ENV_EXTRAWHITE="${BB_ENV_EXTRAWHITE:+$BB_ENV_EXTRAWHITE }$k"
	set -- $k="$v" "$@"
	echo "$k=$v"
done

# variables to be reviewed
if false; then
META_SDK_PATH
TOOLCHAIN_TYPE
TOOLCHAIN_BRAND

BUILD_MODE
POSTFIX
KERNEL_VERSION

TASK_HANOVER_COMMON
TASK_HANOVER_PRODUCT
TASK_HANOVER_PRODUCT_LIBS
fi

# variables to be passed even if empty
for k in \
	LIB_BUILD_MODE \
	; do

	eval "v=\"\${$k:-}\""
	BB_ENV_EXTRAWHITE="$BB_ENV_EXTRAWHITE $k"
	set -- $k="$v" "$@"
done

# variables to be passed if not empty
for k in \
	OEBASE OE_TOP ARAGO_TOP \
	HANOVER_SYSTEM_DEV HANOVER_SYSTEM \
	HANOVER_APPS_DEV HANOVER_APPS \
	HANOVER_PRODUCT \
	DL_DIR \
	http_proxy https_proxy ftp_proxy no_proxy \
	GIT_PROXY_COMMAND \
	SVN_USER SVN_PASSWD HANOVER_SVN \
	HANOVER_GIT HANOVER_GIT_LEGACY \
	HANOVER_RELEASES HANOVER_MIRROR \
	; do

	eval "v=\"\${$k:-}\""
	if [ -n "$v" ]; then
		BB_ENV_EXTRAWHITE="$BB_ENV_EXTRAWHITE $k"
		set -- $k="$v" "$@"
	fi
done

# extra env from setenv we pass just in case
for k in \
	HOME \
	EXPORT_BASE_PATH LOCAL_BASE_PATH \
	FTP_SERVER FTP_BASE_PATH \
	TFTP_SERVER TFTP_BASE_PATH \
	NFS_SERVER NFS_BASE_PATH \
	HTTP_SERVER HTTP_BASE_PATH \
	UPDATESRC \
	; do
	eval "v=\"\${$k:-}\""
	BB_ENV_EXTRAWHITE="$BB_ENV_EXTRAWHITE $k"
	set -- $k="$v" "$@"
done

cd "$OEBASE"
exec env -i \
	PATH="$PATH" \
	BB_ENV_EXTRAWHITE="$BB_ENV_EXTRAWHITE" \
	"$@"
